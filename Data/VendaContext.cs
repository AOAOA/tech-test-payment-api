using Microsoft.EntityFrameworkCore;
using TechTestPaymentApi.Models;

public class VendaContext : DbContext
{
    public VendaContext (DbContextOptions<VendaContext> options)
        : base(options)
    {
    }

    public DbSet<Venda> Vendas { get; set; } = default!;
    public DbSet<Vendedor> Vendedores { get; set; } = default!;
}

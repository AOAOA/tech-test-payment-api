using Microsoft.EntityFrameworkCore;
using TechTestPaymentApi.Models;
using Microsoft.Extensions.DependencyInjection;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddDbContext<VendaContext>(options =>
    options.UseSqlServer(System.Environment.GetEnvironmentVariable("MSSQL_CONNECTION_STRING") ?? throw new InvalidOperationException("Variavel com string de conexao ao DB nao encontrada.")));

// Adiciona servicos ao container
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configura o fluxo da requisição HTTP
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

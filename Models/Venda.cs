namespace TechTestPaymentApi.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public string data { get; set; } = null!;
        public string listaDeItens { get; set; } = null!;
        public string status { get; set; } = null!;

        public Vendedor Vendedor { get; set; } = null!;
    }
}

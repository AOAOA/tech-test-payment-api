using Microsoft.EntityFrameworkCore;

namespace TechTestPaymentApi.Models
{
    [Index(nameof(cpf), IsUnique = true)]
    public class Vendedor
    {
        public int Id { get; set; }
        public string cpf { get; set; } = null!;
        public string nomeBatismo { get; set; } = null!;
        public string? nomeSocial { get; set; }
        public string email { get; set; } = null!;
        public string telefone { get; set; } = null!;
    }
}
